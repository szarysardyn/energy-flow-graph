import dash
import dash_core_components as dcc
import dash_html_components as html
import h5py
import networkx as nx
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from addEdge import addEdge
from dash.dependencies import Input, Output


class Dataset:
    def __init__(self):
        self.data = self.get_dataset()

    def __call__(self, hour):
        return self.get_graph_data(hour)

    @staticmethod
    def get_dataset():
        data = h5py.File("./task_data.hdf5", 'r')
        return data

    def get_hour_results(self, hour):
        nodes = pd.DataFrame(data=np.array(self.data['results']['hour_' + hour]['nodes']),
                             columns=['node_id', 'node_type', 'demand [MW]'])
        gens = pd.DataFrame(data=np.array(self.data['results']['hour_' + hour]['gens']),
                            columns=['node_id', 'generation [MW]', 'cost [zl]'])
        branches = pd.DataFrame(data=np.array(self.data['results']['hour_' + hour]['branches']),
                                columns=['node_from', 'node_to', 'flow [MW]'])
        return nodes, gens, branches

    def get_graph_data(self, hour):
        nodes, gens, branches = self.get_hour_results(hour)
        branches = {'edges': branches.to_numpy()[:, :2], 'flow': branches.to_numpy()[:, 2]}
        return branches, nodes, gens


# Plotly figure
def networkGraph(hour):
    branches, nodes, gens = Dataset()(hour)
    G = nx.Graph()
    for edge, data in zip(branches['edges'], branches['flow']):
        G.add_edge(edge[0], edge[1], data=data)
    pos = nx.spring_layout(G, seed=13)

    gens_id = gens.node_id.values

    # edges trace
    neg_edge_x = []
    neg_edge_y = []

    for edge in [x for x in G.edges(data=True) if x[2]['data'] <= 0]:
        end = pos[edge[0]]
        start = pos[edge[1]]
        neg_edge_x, neg_edge_y = addEdge(start, end, neg_edge_x, neg_edge_y, .8, 'end', .04, 30, 20)

    #print(neg_edge_x, neg_edge_y)
    neg_edge_trace = go.Scatter(
        x=neg_edge_x, y=neg_edge_y,
        line=dict(color='blue', width=1),
        hoverinfo='text',
        showlegend=False,
        mode='lines')

    pos_edge_x = []
    pos_edge_y = []
    for edge in [x for x in G.edges(data=True) if x[2]['data'] > 0]:
        end = pos[edge[0]]
        start = pos[edge[1]]
        pos_edge_x, pos_edge_y = addEdge(start, end, pos_edge_x, pos_edge_y, .8, 'end', .04, 30, 20)

    #print(pos_edge_x, pos_edge_y)
    pos_edge_trace = go.Scatter(
        x=pos_edge_x, y=pos_edge_y,
        line=dict(color='red', width=1),
        hoverinfo='text',
        showlegend=False,
        mode='lines')

    # nodes trace
    node_x = []
    node_y = []
    text = []
    for node in [x for x in G.nodes() if x not in gens_id]:
        x, y = pos[node]
        node_x.append(x)
        node_y.append(y)
        demand = nodes.loc[nodes['node_id'] == node]['demand [MW]'].values
        text.append(str(node) + '\nDemand: ' + str(round(demand[0], 2)) + ' [MW]')

    node_trace = go.Scatter(
        x=node_x, y=node_y, text=text,
        textposition='top center',
        mode='markers+text',
        showlegend=False,
        hoverinfo='text',
        marker=dict(
            color='black',
            size=10,
            line=dict(color='black', width=1)))

    gens_x = []
    gens_y = []
    text = []
    for node in [x for x in G.nodes() if x in gens_id]:
        x, y = pos[node]
        gens_x.append(x)
        gens_y.append(y)
        gen_output = gens.loc[gens['node_id'] == node]['generation [MW]'].values
        text.append("GEN "+str(node) + '\nGeneration: ' + str(round(gen_output[0], 2)) + ' [MW]')

    gens_trace = go.Scatter(
        x=gens_x, y=gens_y, text=text,
        textposition='top center',
        mode='markers+text',
        showlegend=False,
        hoverinfo='text',
        marker=dict(
            color='red',
            size=20,
            line=dict(color='black', width=1)))

    # layout
    layout = dict(plot_bgcolor='white',
                  paper_bgcolor='white',
                  margin=dict(t=10, b=10, l=10, r=10, pad=0),
                  xaxis=dict(linecolor='black',
                             showgrid=False,
                             showticklabels=False,
                             mirror=True),
                  yaxis=dict(linecolor='black',
                             showgrid=False,
                             showticklabels=False,
                             mirror=True))

    # figure
    fig = go.Figure(data=[pos_edge_trace, neg_edge_trace, gens_trace, node_trace], layout=layout)

    return fig


# Dash app
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.title = 'Dash Networkx'

app.layout = html.Div([
        html.H1('Visualizing the flow of the energy network'),
        html.I('Specify the hour (1-24):'),
        html.Br(),
        dcc.Input(id='hour', type='text', value='1', debounce=True),
        dcc.Graph(id='my-graph'),
    ]
)


@app.callback(
    Output('my-graph', 'figure'),
    [Input('hour', 'value')],
)
def update_output(hour):
    return networkGraph(hour)


if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0', port=1234)